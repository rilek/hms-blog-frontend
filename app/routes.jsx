import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Layout from './containers/Layout/Layout';
import Header from './containers/Layout/Header';
import Footer from './containers/Layout/Footer';
import Error404 from './containers/error404/error404';
import Example from './views/Example/Example';


const Main = () => (
  <Layout>
    <Header />
    <Route path="/" exact component={Example}></Route>
    <Footer />
  </Layout>
);

const Routes = () => (
  <Switch>
    <Main />

    <Route component={Error404} />
  </Switch>
);

export default Routes;