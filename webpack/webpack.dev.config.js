const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');

const parentDir = path.join(__dirname, '..', 'app');

module.exports = {
    entry: [
        path.join(parentDir, 'index.jsx')
    ],
    module: {
        loaders: [{
            test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },{
                test: /\.scss$/,
                loaders: ["style-loader", "css-loader", "sass-loader"]
            }
        ]
    },
    output: {
        path: parentDir + '/dist',
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: parentDir,
        historyApiFallback: true
    },
    target: "web",
    resolve: { extensions: [".js", ".jsx"] },
    plugins: [
      new webpack.LoaderOptionsPlugin({
        debug: true,
        noInfo: false,
        postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ],
      }),
      new ExtractTextPlugin("assets/styles.css"),
      new CopyWebpackPlugin([ { from: "./statics", allChunks: true} ]),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        },
      })
    ],
}